<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "personajes".
 *
 * @property int $Identificador
 * @property string $Nombre
 * @property string $Apellidos
 * @property int $Casa
 *
 * @property Casas $casa
 */
class Personajes extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'personajes';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['Nombre', 'Apellidos', 'Casa'], 'required'],
            [['Casa'], 'integer'],
            [['Nombre', 'Apellidos'], 'string', 'max' => 255],
            [['Casa'], 'exist', 'skipOnError' => true, 'targetClass' => Casas::className(), 'targetAttribute' => ['Casa' => 'identificador']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'Identificador' => 'Identificador',
            'Nombre' => 'Nombre',
            'Apellidos' => 'Apellidos',
            'Casa' => 'Casa',
        ];
    }

    /**
     * Gets query for [[Casa]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCasa()
    {
        return $this->hasOne(Casas::className(), ['identificador' => 'Casa']);
    }
}
