<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "reinos".
 *
 * @property int $Identificador
 * @property string $Nombre
 * @property int $Gobernante
 *
 * @property Casas $gobernante
 */
class Reinos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'reinos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['Nombre', 'Gobernante'], 'required'],
            [['Gobernante'], 'integer'],
            [['Nombre'], 'string', 'max' => 255],
            [['Gobernante'], 'exist', 'skipOnError' => true, 'targetClass' => Casas::className(), 'targetAttribute' => ['Gobernante' => 'identificador']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'Identificador' => 'Identificador',
            'Nombre' => 'Nombre',
            'Gobernante' => 'Gobernante',
        ];
    }

    /**
     * Gets query for [[Gobernante]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getGobernante()
    {
        return $this->hasOne(Casas::className(), ['identificador' => 'Gobernante']);
    }
}
