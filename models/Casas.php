<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "casas".
 *
 * @property int $identificador
 * @property string $Nombre
 * @property string $Descripcion
 * @property int $Activa
 *
 * @property Personajes[] $personajes
 * @property Reinos[] $reinos
 */
class Casas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'casas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['Nombre', 'Descripcion', 'Activa'], 'required'],
            [['Activa'], 'integer'],
            [['Nombre', 'Descripcion'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'identificador' => 'Identificador',
            'Nombre' => 'Nombre',
            'Descripcion' => 'Descripcion',
            'Activa' => 'Activa',
        ];
    }

    /**
     * Gets query for [[Personajes]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPersonajes()
    {
        return $this->hasMany(Personajes::className(), ['Casa' => 'identificador']);
    }

    /**
     * Gets query for [[Reinos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getReinos()
    {
        return $this->hasMany(Reinos::className(), ['Gobernante' => 'identificador']);
    }
}
