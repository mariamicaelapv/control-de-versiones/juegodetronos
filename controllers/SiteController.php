<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use yii\data\ActiveDataProvider;
use yii\data\SqlDataProvider;
use app\models\Reinos;
use app\models\Personajes;
use app\models\Casas;


class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
      public function actionConsulta1() {
        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT nombre,apellidos FROM personajes ORDER BY nombre,apellidos',
            'pagination'=>[
                'pageSize' => 5,
                ]
            ]);
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nombre','apellidos'],
            "titulo"=>"Consulta 1 con DAO",
            "enunciado"=>"Listar todos los personajes en orden alfabetico. Primero por nombre y despues por apellido.",
            "sql"=>"SELECT nombre, apellidos FROM personajes ORDER BY nombre, apellidos",
            ]);
    }
    public function actionConsulta2() {
        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT COUNT(*) AS Reinos_totales FROM reinos',
            'pagination'=>[
                'pageSize' => 5,
                ]
            ]);
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['Reinos_totales'],
            "titulo"=>"Consulta 2 con DAO",
            "enunciado"=>"Contar todos los reinos",
            "sql"=>"SELECT COUNT(*) AS Reinos_totales FROM reinos",
            ]);
    }
      public function actionConsulta3() {
        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT COUNT(*) AS Personajes_STARK_LANNISTER FROM personajes WHERE UPPER(apellidos) ="STARK" OR UPPER(apellidos) ="LANNISTER"',
            'pagination'=>[
                'pageSize' => 5,
                ]
            ]);
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['Personajes_STARK_LANNISTER'],
            "titulo"=>"Consulta 3 con DAO",
            "enunciado"=>"Contar todos los personajes que se apelliden stark o lannister",
            "sql"=>"SELECT COUNT(*) AS Reinos_totales FROM reinos",
            ]);
      }
        public function actionConsulta4() {
        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT c.nombre, COUNT(p.nombre) numero_personajes  FROM personajes p INNER JOIN casas c ON c.identificador=p.casa GROUP BY c.identificador ORDER BY COUNT(p.nombre) DESC',
            'pagination'=>[
                'pageSize' => 5,
                ]
            ]);
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nombre','numero_personajes'],
            "titulo"=>"Consulta 4 con DAO",
            "enunciado"=>" Contar todos los personajes de cada casa.",
            "sql"=>"SELECT c.nombre, COUNT(p.nombre) Personajes FROM personajes p INNER JOIN casas c ON c.identificador=p.casa GROUP BY c.identificador ORDER BY COUNT(p.nombre) DESC. <br/> El resultado que nos ofrece la consulta es mostrarnos el total de personajes de cada casa independientemente del apellido ",
            ]);
    }
    public function actionConsulta5() {
        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT p.apellidos, COUNT(p.apellidos) Numero_apellidos FROM personajes p GROUP BY Apellidos ORDER BY COUNT(p.apellidos) DESC, Numero_apellidos',
            'pagination'=>[
                'pageSize' => 5,
                ]
            ]);
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['apellidos','Numero_apellidos'],
            "titulo"=>"Consulta 5 con DAO",
            "enunciado"=>" Contar apellidos repetidos.",
            "sql"=>"SELECT p.apellidos, COUNT(p.apellidos) Numero_apellidos FROM personajes p GROUP BY Apellidos ORDER BY COUNT(p.apellidos) DESC, Numero_apellidos. <br/> La consulta muestra el total de apellidos repetidos indepentiente de la casa del personaje.",
            ]);
    }
     public function actionConsulta6() {
        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT CASE WHEN p.Sexo = "F" THEN "Femenino" ELSE "Masculino" END Sexo, COUNT(p.Sexo) Numero_personajes FROM personajes p GROUP BY p.Sexo',
            'pagination'=>[
                'pageSize' => 5,
                ]
            ]);
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['Sexo','Numero_personajes'],
            "titulo"=>"Consulta 6 con DAO",
            "enunciado"=>" Contar el numero de personajes Femeninos y masculinos",
            "sql"=>"SELECT CASE WHEN p.Sexo = 'F' THEN 'Femenino' ELSE 'Masculino' END Sexo, COUNT(p.Sexo) Numero_personajes FROM personajes p GROUP BY p.Sexo",
            ]);
    }
     public function actionConsulta7() {
        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT COUNT(p.Nombre) AS Nombres_con_F_o_M FROM personajes p WHERE p.Nombre LIKE "%f%" OR p.Nombre LIKE "%m%"',
            'pagination'=>[
                'pageSize' => 5,
                ]
            ]);
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['Nombres_con_F_o_M'],
            "titulo"=>"Consulta 7 con DAO",
            "enunciado"=>" Contar el numero de personajes Femeninos y masculinos",
            "sql"=>" SELECT COUNT(p.Nombre) AS Nombres_con_F_o_M FROM personajes p WHERE p.Nombre LIKE '%f%' OR p.Nombre LIKE '%m%';",
            ]);
    }
    public function actionConsulta8() {
        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT r.Nombre AS Reino, c.Nombre AS Casa_perteneciente FROM Reinos r INNER JOIN casas c ON c.Identificador = r.Gobernante',
            'pagination'=>[
                'pageSize' => 5,
                ]
            ]);
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['Reino','Casa_perteneciente'],
            "titulo"=>"Consulta 8 con DAO",
            "enunciado"=>"  Mostar la casa gobernante de cada reino",
            "sql"=>" SELECT r.Nombre AS Reino, c.Nombre AS Casa_perteneciente FROM Reinos r INNER JOIN casas c ON c.Identificador = r.Gobernante",
            ]);
    }
    public function actionConsulta9() {
        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT r.nombre AS Reino, CONCAT(p.nombre, " ", p.apellidos) AS Gobernante FROM reinos r INNER JOIN casas c ON c.identificador = r.Gobernante INNER JOIN personajes p ON p.Identificador = c.Lider',
            'pagination'=>[
                'pageSize' => 5,
                ]
            ]);
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['Reino','Gobernante'],
            "titulo"=>"Consulta 9 con DAO",
            "enunciado"=>"Mostar el personaje gobernante de cada reino ",
            "sql"=>"SELECT r.nombre AS Reino, CONCAT(p.nombre, ' ', p.apellidos) AS Gobernante FROM reinos r INNER JOIN casas c ON c.identificador = r.Gobernante INNER JOIN personajes p ON p.Identificador = c.Lider; ",
            ]);
    }
    public function actionConsulta10() {
        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT r.nombre AS Reino, c.Nombre AS Casa, CONCAT(p.nombre, " ", p.apellidos) AS Gobernante FROM reinos r INNER JOIN casas c ON c.identificador = r.Gobernante INNER JOIN personajes p ON p.Identificador = c.Lider WHERE c.Activa != 0',
            'pagination'=>[
                'pageSize' => 5,
                ]
            ]);
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['Reino','Casa','Gobernante'],
            "titulo"=>"Consulta 10 con DAO",
            "enunciado"=>" Mostar el nombre del reino, el nombre de la casa y el nombre del personaje gobernante solo cuando la casa este activa ",
            "sql"=>"SELECT r.nombre AS Reino, c.Nombre AS Casa, CONCAT(p.nombre, ' ', p.apellidos) AS Gobernante FROM reinos r INNER JOIN casas c ON c.identificador = r.Gobernante INNER JOIN personajes p ON p.Identificador = c.Lider WHERE c.Activa != 0",
            ]);
    }
}
