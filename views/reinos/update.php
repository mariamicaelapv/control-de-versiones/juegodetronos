<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Reinos */

$this->title = 'Update Reinos: ' . $model->Identificador;
$this->params['breadcrumbs'][] = ['label' => 'Reinos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->Identificador, 'url' => ['view', 'id' => $model->Identificador]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="reinos-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
