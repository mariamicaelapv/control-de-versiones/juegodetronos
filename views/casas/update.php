<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Casas */

$this->title = 'Update Casas: ' . $model->identificador;
$this->params['breadcrumbs'][] = ['label' => 'Casas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->identificador, 'url' => ['view', 'id' => $model->identificador]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="casas-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
