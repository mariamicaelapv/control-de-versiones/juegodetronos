<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Casas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="casas-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Casas', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'identificador',
            'Nombre',
            'Descripcion',
            'Activa',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
