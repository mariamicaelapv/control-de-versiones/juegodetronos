<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Casas */

$this->title = 'Create Casas';
$this->params['breadcrumbs'][] = ['label' => 'Casas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="casas-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
