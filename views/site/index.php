<?php

use yii\helpers\Html;


$this->title = 'Consultas Juego de tronos';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Consultas Juego de tronos</h1>
    </div>

    <div class="body-content">
        <div class="row">
            <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <div class="caption">
                        <h3>Consulta 1</h3>
                        <p> Listar todos los personajes en orden alfabetico. Primero por nombre y despues por apellido.</p>
                        <p>
                          <?= Html::a('DAO', ['site/consulta1'], ['class' => 'btn btn-default']) ?>
                        </p>    
                    </div>
                </div>
            </div>
            
                <div class="col-sm-6 col-md-4">
                    <div class="thumbnail">
                        <div class="caption">
                            <h3>Consulta 2</h3>
                            <p>  Contar todos los reinos. </p>
                            <p>
                                    
                                    <?= Html::a('DAO', ['site/consulta2'], ['class' => 'btn btn-default']) ?>
                            </p>
                        </div>
                    </div>
                </div>
                                    <div class="col-sm-6 col-md-4">
                        <div class="thumbnail">
                            <div class="caption">
                                <h3>Consulta 3</h3>
                                <p>  Contar todos los personajes que se apelliden stark o lannister </p>
                                <p>
                                    
                                    <?= Html::a('DAO', ['site/consulta3'], ['class' => 'btn btn-default']) ?>
                                </p>
                            </div>
                        </div>
                    </div>
                           <div class="col-sm-6 col-md-4">
                            <div class="thumbnail">
                                <div class="caption">
                                    <h3>Consulta 4</h3>
                                    <p> Contar todos los personajes de cada casa.</p>
                                    <p>
                                       
                                       <?= Html::a('DAO', ['site/consulta4'], ['class' => 'btn btn-default']) ?>

                                    </p>
                                </div>
                            </div>
                        </div>
                       
                        <div class="col-sm-6 col-md-4">
                            <div class="thumbnail">
                                <div class="caption">
                                    <h3>Consulta 5</h3>
                                    <p>Contar apellidos repetidos.</p>
                                    <p>
                                       
                                        <?= Html::a('DAO', ['site/consulta5'], ['class' => 'btn btn-default']) ?>
                                       

                                    </p>
                                </div>
                            </div>
                        </div>
                            <div class="row">
                        <div class="col-sm-6 col-md-4">
                            <div class="thumbnail">
                                <div class="caption">
                                    <h3>Consulta 6</h3>
                                    <p>Contar el numero de personajes Femeninos y masculinos</p>
                                    <p>
                                       
                                       <?= Html::a('DAO', ['site/consulta6'], ['class' => 'btn btn-default']) ?>
                                       

                                    </p>
                                </div>
                            </div>
                        </div>
                                
                       
                        <div class="col-sm-6 col-md-4">
                            <div class="thumbnail">
                                <div class="caption">
                                    <h3>Consulta 7</h3>
                                    <p>Contar los personajes que en su nombre contengan la F o M</p>
                                    <p>
                                       
                                        <?= Html::a('DAO', ['site/consulta7'], ['class' => 'btn btn-default']) ?>
                                     
                                    </p>
                                </div>
                            </div>
                        </div>
                            
                         
                        <div class="col-sm-6 col-md-4">
                            <div class="thumbnail">
                                <div class="caption">
                                    <h3>Consulta 8</h3>
                                    <p>  Mostrar la casa gobernante de cada reino</p>
                                    <p>
                                       
                                        <?= Html::a('DAO', ['site/consulta8'], ['class' => 'btn btn-default']) ?>
                                     
                                    </p>
                                </div>
                            </div>
                        </div>   
                                 <div class="col-sm-6 col-md-4">
                            <div class="thumbnail">
                                <div class="caption">
                                    <h3>Consulta 9</h3>
                                    <p>  Mostrar el personaje gobernante de cada reino </p>
                                    <p>
                                      
                                        <?= Html::a('DAO', ['site/consulta9'], ['class' => 'btn btn-default']) ?>
                                     
                                    </p>
                                </div>
                            </div>
                        </div>   
  
                                 <div class="col-sm-6 col-md-4">
                            <div class="thumbnail">
                                <div class="caption">
                                    <h3>Consulta 10</h3>
                                    <p> Mostrar el nombre del reino, el nombre de la casa y el nombre del personaje gobernante solo cuando la casa este activa</p>
                                    <p>
                                       
                                        <?= Html::a('DAO', ['site/consulta10'], ['class' => 'btn btn-default']) ?>
                                     
                                    </p>
                                </div>
                            </div>
                        </div>
                            </div>
                               </div> 
         </div>
</div>
        
         