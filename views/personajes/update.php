<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Personajes */

$this->title = 'Update Personajes: ' . $model->Identificador;
$this->params['breadcrumbs'][] = ['label' => 'Personajes', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->Identificador, 'url' => ['view', 'id' => $model->Identificador]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="personajes-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
